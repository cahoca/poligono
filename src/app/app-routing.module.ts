import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CourierOrderComponent } from './components/admin-courier-order/courier-order/courier-order.component';
import { DisplayCourierOrdersComponent } from './components/admin-courier-order/display-courier-orders/display-courier-orders.component';
import { LayoutComponent } from './components/shared/layout/layout.component';
import { DeliveryAdminComponent } from './components/delivery-admin/delivery-admin.component';
import { ChatComponent } from './components/chat/chat.component';
import { ChatsComponent } from './components/chats/chats.component';


const routes: Routes = [
  {
    path: ':IdTercero', component: LayoutComponent, children: [
      { path: ':IdTercero/crear-orden-mensajeria', component: CourierOrderComponent },
      { path: ':IdTercero/ver-ordenes-mensajeria', component: DisplayCourierOrdersComponent },
      { path: ':IdTercero/administracion-mensajeros', component: DeliveryAdminComponent },
      { path: ':IdTercero/sala-mensajes', component: ChatsComponent },
      { path: ':IdTercero/chat', component: ChatComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
