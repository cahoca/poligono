import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserData } from '../models/user-data';

// import { Delivery } from '../models/delivery';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {
  private URLAPI = `${environment.urlApi}/mensajeros`;
  private HEADERS = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private httpClient: HttpClient) { }

  // getUserData(): Observable<Array<Delivery>> {
  //   return this.httpClient.post<Array<Delivery>>(this.URLAPI);
  // }

  getUserData(userData: UserData): Observable<UserData> {
    console.log(userData);
    return this.httpClient.post<UserData>(this.URLAPI, userData, { headers: this.HEADERS });
  }
}