import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { Delivery } from '../models/delivery';

@Injectable({
  providedIn: 'root'
})
export class DeliveriesService {
  private URLAPI = `${environment.urlApi}/mensajeros`;
  private HEADERS = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private httpClient: HttpClient) { }

  getDeliveries(): Observable<Array<Delivery>> {
    return this.httpClient.get<Array<Delivery>>(this.URLAPI);
  }

  updateDelivery(delivery: Delivery): Observable<Delivery> {
    return this.httpClient.put<Delivery>(this.URLAPI, delivery, { headers: this.HEADERS });
  }
}