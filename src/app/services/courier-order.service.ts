import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { CourierOrder } from '../models/courier-order';
import { CourierOrderAssign } from '../models/courier-order-assign';

@Injectable({
  providedIn: 'root'
})
export class CourierOrderService {
  private URLAPI = `${environment.urlApi}/ordenesmensajeria`;
  private HEADERS = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private httpClient: HttpClient) { }

  getOrders(): Observable<Array<CourierOrder>> {
    return this.httpClient.get<Array<CourierOrder>>(this.URLAPI);
  }

  insertOrder(order: CourierOrder) {
    return this.httpClient.post(this.URLAPI, order, { headers: this.HEADERS });
  }

  updateOrder(assign: CourierOrderAssign, idOperator: number) {
    return this.httpClient.put(`${this.URLAPI}?idOperator=${idOperator}`, assign);
  }

  deleteOrder(idOrder: number, observation: string) {
    return this.httpClient.delete(`${this.URLAPI}/${idOrder}/${observation}`);
  }
}
