import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styles: []
})
export class ChatComponent {

  constructor(private _location: Location) { }

  goBack() {
    this._location.back();
  }

}
