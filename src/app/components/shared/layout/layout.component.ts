import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { UserData } from 'src/app/models/user-data';
import { UserDataService } from 'src/app/services/user-data.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html'
})
export class LayoutComponent implements OnInit {
  IdTerceroR = "";

 
  constructor(private route: ActivatedRoute,
    private router: Router,
    private usDataService: UserDataService,
    private userData: UserData) { }

  ngOnInit() {
    this.IdTerceroR = this.route.snapshot.paramMap.get("IdTercero");
    this.guardarData();
    // Subscribed
    // this.route.paramMap.subscribe(params => {
    //   this.subscribedParam = params.get("IdTercero");
    //   console.log(this.subscribedParam);
    // });
  
  }

  guardarData(){
  //  this.userdata.idTercero = this.IdTerceroR;
    this.userData.guardarInfo(this.IdTerceroR, undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined);

    this.usDataService.getUserData(this.userData.obtenerInfo()[0])
      .subscribe(data => {
        if (data) {
          this.userData.guardarInfo(this.IdTerceroR, data[0].id, data[0].nombre, data[0].zona, data[0].estado, data[0].telefono, data[0].extension, data[0].pathImage, data[0].zona_nombre);
          console.log(this.userData.obtenerInfo());
        } else {
          //this.toasterService.error('Ha ocurrido un error al realizar la creación, valide los datos e intentelo nuevamente');
        }
      }, err => {
        console.error(err)
       // this.toasterService.error('Ha ocurrido un error al realizar la creación, valide los datos e intentelo nuevamente');
      });
  }

}
