import { Component } from '@angular/core';
import { UserData } from 'src/app/models/user-data';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: []
})

export class SidebarComponent {
  constructor(public userData: UserData){}
 }
