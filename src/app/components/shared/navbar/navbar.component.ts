import { Component, ViewChild, ElementRef, Renderer2, HostListener, Input, HostBinding } from '@angular/core';
// import { fromEvent } from 'rxjs/observable/fromEvent';
import { UserData } from 'src/app/models/user-data';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent {
  @ViewChild('noti', { static: false }) noti: ElementRef;
  @Input() appImgFallback: string;

  classToShow: string;
  error:any;
  constructor(private renderer: Renderer2,
    public userData: UserData, private eRef: ElementRef) {
    this.classToShow = "";
    this.renderer.listen('window', 'click', (e: Event) => {
      if (e.target !== this.noti.nativeElement) {
        this.classToShow = "";
      }
    });
  }

  ngAfterContentInit() {
  }

  @HostListener('error')
  loadFallbackOnError() {
    console.log('erroaqr');
    const element: HTMLImageElement = <HTMLImageElement>this.eRef.nativeElement;
    element.src = this.appImgFallback || 'https://via.placeholder.com/200';
  }

  toggleNotifications() {
    if (this.classToShow === "") {
      this.classToShow = "show-dropdown"
    } else {
      this.classToShow = ""
    }
  }

}
