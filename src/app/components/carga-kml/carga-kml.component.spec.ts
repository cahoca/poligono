import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CargaKmlComponent } from './carga-kml.component';

describe('CargaKmlComponent', () => {
  let component: CargaKmlComponent;
  let fixture: ComponentFixture<CargaKmlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CargaKmlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CargaKmlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
