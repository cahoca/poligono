import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

import { Delivery } from '../../models/delivery';
import { DeliveriesService } from '../../services/deliveries.service';
import { UserData } from 'src/app/models/user-data';
import { Router } from '@angular/router';


@Component({
  selector: 'app-delivery-admin',
  templateUrl: './delivery-admin.component.html',
  styles: []
})
export class DeliveryAdminComponent implements OnInit {
  deliveries: Array<Delivery>;
  deliveryToUpdate: Delivery;
  editZona: number;
  editState: string;

  constructor(private deliveriesService: DeliveriesService, private toasterService: ToastrService, private userData: UserData,
              private route: Router
  ) { }

  ngOnInit() {
    setTimeout(() => {
      if (this.userData.obtenerInfo()[8] == "ADMINISTRADOR") {
        this.getDeliveries();
      } else {
        Swal.fire({
          title: 'No tiene permisos',
          text: 'No tiene privilegios para este sitio',
          type: 'error',
          confirmButtonText: 'Aceptar'
        }).then((res) => {
          if (res.value) {
            this.route.navigateByUrl('/' + this.userData.obtenerInfo()[0]);
            //console.log('Aceptó');
          }
        });
      }
  }, 200);
    
  }

  getDeliveries(): void {
    this.deliveriesService.getDeliveries().subscribe((result) => {
      this.deliveries = result;
    });
  }

  getClassState(state: string): string {
    switch (state) {
      case 'Activo':
        return 'success';
      case 'Ausente':
        return 'secondary';
      case 'Inactivo':
        return 'danger';
    }
  }

  setToUpdate(delivery: Delivery): void {
    this.deliveryToUpdate = delivery;
    this.editState = this.deliveryToUpdate.estado;
    this.editZona = this.deliveryToUpdate.zona;
  }

  confirmUpdate(): void {
    Swal.fire({
      title: '¿Está seguro(a)?',
      text: 'Si desea continuar de clic en confirmar',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Confirmar'
    }).then((res) => {
      if (res.value) {
        this.deliveryToUpdate.estado = this.editState;
        this.deliveryToUpdate.zona = this.editZona;
        this.deliveriesService.updateDelivery(this.deliveryToUpdate).subscribe(() => {
          this.toasterService.success('Se ha realizado la actualización correctamente.');
        });
      }
    });
  }
}
