import { MapsAPILoader, MouseEvent } from '@agm/core';
import { Component, ElementRef, NgZone, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

import { OrderType } from '../../../enums/order-type.enum';
import { CourierOrder } from '../../../models/courier-order';
import { CourierOrderItem } from '../../../models/courier-order-item';
import { CourierOrderService } from '../../../services/courier-order.service';
import * as KmlCapa from '../../../data/kml-layer';


@Component({
  selector: "app-courier-order",
  templateUrl: "./courier-order.component.html",
  styles: ["#map{height:350px;width:100%;}"]
})
export class CourierOrderComponent implements OnInit {
  @ViewChild("search", { static: true }) searchElementRef: ElementRef;

  zoom: number;
  private geoCoder: any;
  time = { hour: 13, minute: 30 };

  courierOrder: CourierOrder;

  elementToAdd: CourierOrderItem;
  elementsList: any[] = [
    "Carta",
    "CCCF/T Credito",
    "Cheque",
    "Correspondencia",
    "Facturas",
    "Folletos",
    "Otros",
    "Sobre",
    "Voucher Hotel"
  ];

  owner: string;
  isValid: boolean;
  itemValid: boolean;

  indexToEdit: number;
  elementToEdit: CourierOrderItem;

  ngDate: any;
  year: number;
  month: number;
  day: number;

  Zonas = KmlCapa.L.kml.Document.Folder;
  LatitudMarker: number;
  LongitudMarker: number;
  orderTypes = OrderType;
  orderTypesId: any;
  polygon: any;
  PuntoSeleccionado: any; // Objeto google.maps.LatLng(latitud,Longitud)
  Polygons: any = [10]; // Array polígonos google.maps.Polygon({lat:number,Lng:number}[])
  map: any; // Objeto google.maps.Map
  center: any; // Objeto LatLng que centra el mapa y el marker
  marker: any; //Objeto Marker

  /*------- VARIABLE ZONA --------*/
  ZonaSeleccionada: any; //Cadena que contiene la zona seleccionada apartir de busquedas
  /*------ VARIABLE ZONA ------*/

  constructor(
    private courierService: CourierOrderService,
    private toasterService: ToastrService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) {
    this.restartCourierOrder();
    this.restartElement();
    this.owner = "owner";
    this.isValid = false;
    this.itemValid = false;

    this.orderTypesId = Object.keys(this.orderTypes).filter(Number);
  }
 

  ngOnInit() {
    // console.log('Administracion de ordenes cargada');
    this.getMinDate();
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      console.log(this.Zonas);
      this.geoCoder = new google.maps.Geocoder;
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      this.geoCoder = new google.maps.Geocoder();
      for (let n = 0; n < KmlCapa.L.kml.Document.Folder.length; n++) {
        this.Polygons[n] = new google.maps.Polygon({
          paths:
            KmlCapa.L.kml.Document.Folder[n].Placemark.Polygon.outerBoundaryIs
              .LinearRing.coordinates,
          geodesic: false,
          strokeColor:
            KmlCapa.L.kml.Document.Folder[n].Placemark.Polygon.outerBoundaryIs
              .LinearRing.LineStyle.color,
          strokeOpacity: 0.45,
          strokeWeight: 2,
          fillColor:
            KmlCapa.L.kml.Document.Folder[n].Placemark.Polygon.outerBoundaryIs
              .LinearRing.LineStyle.color,
          fillOpacity: 0.1,
          clickable: false
        });
      }
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          //set latitude, longitude and zoom
          this.courierOrder.latitud = place.geometry.location.lat();
          this.courierOrder.longitud = place.geometry.location.lng();
          this.LatitudMarker = place.geometry.location.lat();
          this.LongitudMarker = place.geometry.location.lng();
          this.getAddress(this.courierOrder.latitud, this.courierOrder.longitud);
          this.center = new google.maps.LatLng(
            this.courierOrder.latitud,
            this.courierOrder.longitud
          );
          this.marker.setPosition(this.center);
          this.map.setCenter(this.center);
          this.verificarPunto(this.center);
          this.getAddress(
            this.courierOrder.latitud,
            this.courierOrder.longitud
          );
        });
      });
    });
  }

  // Obtiene información para cargar fecha mínima permitida
  getMinDate(city: string = "Bogotá") {
    let date = new Date();
    this.time.hour = date.getHours();
    this.time.minute = date.getMinutes();
    if (city !== "Bogotá") {
      date.setDate(date.getDate() + 1);
    }
    this.year = date.getFullYear();
    this.month = date.getMonth() + 1;
    this.day = date.getUTCDate();
  }

  private setCurrentLocation() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(position => {
        this.courierOrder.latitud = position.coords.latitude;
        this.courierOrder.longitud = position.coords.longitude;
        this.zoom = 14;
        this.getAddress(this.courierOrder.latitud, this.courierOrder.longitud);
        this.center = new google.maps.LatLng(
          this.courierOrder.latitud,
          this.courierOrder.longitud
        );

        this.map = new google.maps.Map(document.getElementById("map"), {
          center: this.center,
          zoom: this.zoom,
          scaleControl: false
        });
        this.marker = new google.maps.Marker({
          position: this.center
        });
        //Asignacion de los polígonos al mapa
        for (let nn = 0; nn < this.Polygons.length; nn++) {
          this.Polygons[nn].setMap(this.map);
        }
        this.marker.setMap(this.map);
        this.marker.setDraggable(true);
        this.marker.addListener("dragend", $event => {
          this.markerDragEnd($event);
        });
        this.marker.addListener("dragend", $event => {
          this.markerDragEnd($event);
        });
        this.map.addListener("click", $event => {
          this.markerDragEnd($event);
        });
      });
    }
  }

  markerDragEnd($event) {
    let lat = Number($event.latLng.lat(1));
    let lng = Number($event.latLng.lng(2));
    this.courierOrder.latitud = lat;
    this.courierOrder.longitud = lng;
    this.PuntoSeleccionado = new google.maps.LatLng(lat, lng);
    this.center = this.PuntoSeleccionado;
    this.marker.setPosition(this.center);
    this.verificarPunto(this.PuntoSeleccionado);
    this.getAddress(lat, lng);
  }

  convertirAMPM(hours,minutes):string{
    let AmOrPm = hours >= 12 ? "PM" : "AM";
    let hora12: string;
    hours = hours % 12 || 12;
    hora12 = hours + ":" + minutes + AmOrPm;
    return hora12;
  }

  verificarPunto(ps: google.maps.LatLng) {
    for (let o = 0; o < this.Polygons.length; o++) {
      if (google.maps.geometry.poly.containsLocation(ps, this.Polygons[o])) {
        this.ZonaSeleccionada = KmlCapa.L.kml.Document.Folder[o].name;
        this.courierOrder.IdZona = this.ZonaSeleccionada;
        console.log(this.courierOrder.IdZona);
        break;
      } else {
        this.ZonaSeleccionada = "SIN_COBERTURA";
        this.courierOrder.IdZona = this.ZonaSeleccionada;
        console.log(this.courierOrder.IdZona);
      }
    }
  }

  getAddress(latitude: any, longitude: any) {
    this.geoCoder.geocode(
      { location: { lat: latitude, lng: longitude } },
      (results: Array<any>, status: string) => {
        if (status === "OK") {
          if (results[0]) {
            let city: string;
            if (results[0].address_components[4]) {
              city = results[0].address_components[4].long_name;
            } else {
              // La primera vez que carga el mapa trae la información en estos campos
              city = results[0].address_components[0].long_name;
            }
            if (city !== this.courierOrder.idCiudad) {
              this.ngDate = null;
            }
            this.courierOrder.idCiudad = city;
            this.courierOrder.direccion = results[0].formatted_address;
            this.getMinDate(this.courierOrder.idCiudad);
          } else {
            window.alert("No se encontraron resultados");
          }
        } else {
          window.alert("Geocoder Fallo: " + status);
        }
      }
    );
  }

  addElement() {
    this.courierOrder.elementos.push(this.elementToAdd);
    this.restartElement();
  }

  editElement(i: number) {
    this.indexToEdit = i;
    this.elementToEdit = this.courierOrder.elementos[this.indexToEdit];
  }

  removeElement(index: number) {
    this.courierOrder.elementos.splice(index, 1);
  }

  restartElement() {
    this.elementToAdd = new CourierOrderItem(0, 0, "", "", null);
    this.elementToEdit = new CourierOrderItem(0, 0, "", "", null);
    this.validNewItem();
  }

  restartCourierOrder() {
    this.courierOrder = new CourierOrder(
      0,
      "",
      new Date(),
      "",
      "",
      "",
      "",
      0,
      0,
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      new Array<CourierOrderItem>(),
      null
    );
  }
  cancel() {
    Swal.fire({
      title: "¿Estás seguro(a)?",
      text: "Al cancelar perderás toda la información diligenciada",
      type: "warning",
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonText: "Confirmar"
    }).then(res => {
      if (res.value) {
        this.restartElement();
        this.restartCourierOrder();
        this.setCurrentLocation();
      }
    });
  }

  saveOrder() {
    // Cambio de fecha del ng date picker a fecha valida
    this.courierOrder.fecha = `${this.ngDate.year}-${this.ngDate.month}-${this.ngDate.day}`;
    Swal.fire({
      title: "Verifica la información",
      text: "¿Deseas crear la orden con la información diligenciada?",
      type: "warning",
      showCancelButton: true,
      cancelButtonText: "Cancelar",
      confirmButtonText: "Confirmar"
    })
      .then(res => {
        if (res.value) {
          this.courierOrder.hora=this.convertirAMPM(this.time.hour,this.time.minute);
          console.log(this.courierOrder.hora);
          this.courierService.insertOrder(this.courierOrder).subscribe(
            data => {
              if (data) {
                this.toasterService.success(
                  "Se ha realizado la creación con exito"
                );
                this.restartElement();
                this.restartCourierOrder();
                this.setCurrentLocation();
                this.ngDate = null;
              } else {
                this.toasterService.error(
                  "Ha ocurrido un error al realizar la creación, valida los datos e inténtalo nuevamente"
                );
              }
            },
            err => {
              console.error(err);
              this.toasterService.error(
                "Ha ocurrido un error al realizar la creación, valida los datos e inténtalo nuevamente"
              );
            }
          );
        }
      })
      .catch(err => console.error(err));
  }

  validNewItem() {
    this.itemValid = false;
    if (
      this.elementToAdd.idTipo != 0 &&
      this.elementToAdd.cantidad > 0 &&
      this.elementToAdd.elemento.trim() != ""
    ) {
      if (this.elementToAdd.elemento === "Otros") {
        if (this.elementToAdd.especifique.trim() !== "") {
          this.itemValid = true;
        }
      } else {
        this.itemValid = true;
      }
    }
  }

  fieldValidation(event: KeyboardEvent, type: string) {
    let re: string;
    let specialKey = false;
    // 8 = BackSpace, 9 Tab, 46 = Supr, 37 = flecha izquierda, 39 = flecha derecha, 32 = Space
    const specialKeys = [8, 9, 37, 39, 46];
    switch (type) {
      case "names":
        re = "^[a-zA-ZÀ-ÿ]+$";
        specialKeys.push(32);
        break;
      case "alpha":
        re = "^[A-Za-z0-9-.@#+-,_-]+";
        specialKeys.push(32);
        break;
      case "number":
        re = "[0-9+]";
        break;
      default:
        break;
    }
    const codCaracter = event.charCode || event.keyCode;
    specialKeys.forEach(element => {
      if (codCaracter === element) {
        specialKey = true;
      }
    });

    const regex = new RegExp(re);
    const key = String.fromCharCode(
      !event.charCode ? event.which : event.charCode
    );
    if (!regex.test(key) && !specialKey) {
      event.preventDefault();
      return false;
    }
  }
}


