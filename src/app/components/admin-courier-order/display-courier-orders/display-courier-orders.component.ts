import { Component, OnInit } from '@angular/core';
import { Select2OptionData } from 'ng-select2';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

import { CourierOrder } from '../../../models/courier-order';
import { Delivery } from '../../../models/delivery';
import { OrderFilter } from '../../../models/order-filter';
import { CourierOrderService } from '../../../services/courier-order.service';
import { DeliveriesService } from '../../../services/deliveries.service';

@Component({
  selector: 'app-display-courier-orders',
  templateUrl: './display-courier-orders.component.html',
  styles: []
})
export class DisplayCourierOrdersComponent implements OnInit {
  objectFilter: OrderFilter;

  courierOrders: Array<CourierOrder>;
  deliveries: Array<Delivery>
  courierOrdersFiltered: Array<CourierOrder>;
  showFilter: boolean;

  orderToAssign: CourierOrder;

  public exampleData: Array<Select2OptionData>;
  public prioridades: Array<Select2OptionData>;
  public zonas: Array<Select2OptionData>;

  public options: any;

  constructor(private courierService: CourierOrderService,
    private toasterService: ToastrService,
    private deliveriesService: DeliveriesService) {
    this.showFilter = false;
    this.objectFilter = new OrderFilter();
  }

  ngOnInit() {
    this.getOrders();
    this.getDeliveries();
    this.fillData();
  }

  // Se obtienen las ordenes de mensajería
  getOrders() {
    this.courierService.getOrders().subscribe(data => {
      this.courierOrders = data;
      this.courierOrdersFiltered = this.courierOrders;
    });
  }

  // Se obtienen los mensajeros para asignación o re asignación
  getDeliveries(): void {
    this.deliveriesService.getDeliveries().subscribe((result) => {
      this.deliveries = result;
    });
  }

  findDelivery(idDelivery: number) {
    return this.deliveries.find(x => x.id == idDelivery).nombre;
  }

  // Llena la data a usar en el filtro
  fillData() {
    this.options = {
      multiple: true
    }

    this.prioridades = [
      {
        id: 'Alta',
        text: 'Alta'
      },
      {
        id: 'Media',
        text: 'Media'
      },
      {
        id: 'Baja',
        text: 'Baja'
      }
    ]
  }

  applyFilter() {
    setTimeout(() => {
      this.courierOrdersFiltered = this.courierOrders;
      if (this.objectFilter.prioridades.length > 0)
        this.courierOrdersFiltered = this.courierOrdersFiltered.filter(x => this.objectFilter.prioridades.includes(x.prioridad))
    }, 1);
  }

  cleanFilter() {
    this.objectFilter.prioridades = [];
    this.applyFilter();
  }

  // Des-selecciona los mensajeros diferentes al seleccionado
  validSelect(delivery: Delivery) {
    if (!delivery.selected)
      this.deliveries.filter(x => x.id !== delivery.id).forEach(d => d.selected = false);
  }

  selectOrderToAssign(courierOrder: CourierOrder) {
    this.cleanAssignDelivery();
    this.orderToAssign = courierOrder;
  }

  cleanAssignDelivery() {
    this.deliveries.forEach((d) => d.selected = false);
  }

  // Se realiza la asignación de los mensajeros
  assignDelivery() {
    Swal.fire({
      title: '¿Está seguro(a)?',
      text: '¿Desea asignar la solicitud al mensajero seleccionado?',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Asignar'
    }).then((res) => {
      if (res.value) {
        const idOperator = this.deliveries.find(x => x.selected).id;
        this.orderToAssign.asignacion.fkOrdenNo = this.orderToAssign.ordenNo;
        this.courierService.updateOrder(this.orderToAssign.asignacion, idOperator).subscribe(() => {
          this.orderToAssign.asignacion.idOperador = idOperator;
          this.toasterService.success(`Se ha asignado la orden correctamente.`);
        });
      }
    });
  }

  cancelOrder(orderId: number) {
    Swal.fire({
      title: '¿Está seguro(a)?',
      text: 'Al anular la orden, no se podrá retomar nuevamente y tendrá que ser creada nuevamente.',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Anular'
    }).then((res) => {
      if (res.value) {
        this.courierService.deleteOrder(orderId, 'Cancelacion desde administracion de ordenes').subscribe(() => {
          this.toasterService.success(`Se ha cancelado la orden no. ${orderId} correctamente.`);
          this.getOrders();
        });
      }
    });
  }
}
