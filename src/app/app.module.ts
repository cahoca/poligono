import { AgmCoreModule } from '@agm/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import localeEs from '@angular/common/locales/es';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbDatepickerModule, NgbTooltipModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelect2Module } from 'ng-select2';
import { ToastrModule } from 'ngx-toastr';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CourierOrderComponent } from './components/admin-courier-order/courier-order/courier-order.component';
import { LayoutComponent } from './components/shared/layout/layout.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { SidebarComponent } from './components/shared/sidebar/sidebar.component';
import { OrderTypePipe } from './pipes/order-type.pipe';
import { DisplayCourierOrdersComponent } from './components/admin-courier-order/display-courier-orders/display-courier-orders.component';
import { DeliveryAdminComponent } from './components/delivery-admin/delivery-admin.component';
import { ChatComponent } from './components/chat/chat.component';
import { ChatsComponent } from './components/chats/chats.component';
import { CargaKmlComponent } from './components/carga-kml/carga-kml.component';
import { UserData } from './models/user-data';

registerLocaleData(localeEs, 'es');

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    SidebarComponent,
    NavbarComponent,
    CourierOrderComponent,
    OrderTypePipe,
    DisplayCourierOrdersComponent,
    DeliveryAdminComponent,
    ChatComponent,
    ChatsComponent,
    CargaKmlComponent
  ],
  imports: [
    NgbModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbTooltipModule,
    NgbDatepickerModule,
    NgSelect2Module,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAoDjU33lmWDOfQUO57rmwl8wpP1SR2fnY',
      libraries: ['places'],
      language: 'es-419',
      region: 'co'
    }),
    NgProgressModule.withConfig({
      spinner: true,
      color: '#041e5b',
      thick: true
    }),
    NgProgressHttpModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
