import { Pipe, PipeTransform } from '@angular/core';
import { OrderType } from '../enums/order-type.enum';

@Pipe({
  name: 'orderType'
})
export class OrderTypePipe implements PipeTransform {

  transform(value: number): any {
    return OrderType[value];
  }
}
