export class Delivery {
    constructor(
        public id: number,
        public idTercero: number,
        public nombre: string,
        public zona: number,
        public estado: string,
        public telefono: string,
        public extension: string,
        public pathImage: string,
        public selected = false // Usado solamente cuando se asigna una solicitud
        ) { }
}