export class CourierOrderAssign {
    constructor(
        public id: number,
        public idOperador: number,
        public idZona: number,
        public idQR: string,
        public idEstado: string,
        public fkOrdenNo: number
    ) { }
}