import { CourierOrderItem } from './courier-order-item';
import { CourierOrderAssign } from './courier-order-assign';

export class CourierOrder {
    constructor(
        public ordenNo: number,
        public prioridad: string,
        public fecha: any,
        public clienteDestino: string,
        public personaAContactar: string,
        public direccion: string,
        public idCiudad: string,
        public latitud: number,
        public longitud: number,
        public telefono: string,
        public personaQuienEnvia: string,
        public area: string,
        public extension: string,
        public observaciones: string,
        public nombreQuienRecibe: string,
        public hora: string,
        public IdZona: string,
        public elementos: Array<CourierOrderItem>,
        public asignacion: CourierOrderAssign,
        public showDetails = false // Usado solamente cuando se listan todas las ordenes de mensajeria para ver los detalles de cada una
    ) { }
}