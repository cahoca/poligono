import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})

export class UserData {
    idTercero: any;
    id: any;
    nombre: any;
    zona: any;
    estado: any;
    telefono: any;
    extension: any;
    pathImage: any;
    zona_nombre: any;

    guardarInfo(idTercero, id, nombre, zona, estado, telefono, extension, pathImage, zona_nombre){
        this.idTercero=idTercero;
        this.id=id;
        this.nombre=nombre;
        this.zona=zona;
        this.estado=estado;
        this.telefono=telefono;
        this.extension=extension;
        this.pathImage=pathImage;
        this.zona_nombre=zona_nombre;
    }

    obtenerInfo():any{
        return [this.idTercero, this.id, this.nombre, this.zona, this.estado, this.telefono, this.extension, this.pathImage, this.zona_nombre];
    }
}


