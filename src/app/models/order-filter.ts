export class OrderFilter {
    constructor(
        public prioridades: Array<string> = [],
        public mensajeros: Array<string> = [],
        public zonas: Array<string> = [],
        public ciudades: Array<string> = []
    ) { }
}