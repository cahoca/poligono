export class CourierOrderItem {
    constructor(
        public id: number,
        public idTipo: number,
        public elemento: string,
        public especifique: string,
        public cantidad: number
    ) { }
}